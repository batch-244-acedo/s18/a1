// Item 1


function printSum(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of 5 and 15:");
	console.log(sum);
};
printSum(5, 15);


function printDifference(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed difference of 20 and 5:");
	console.log(difference);
};
printDifference(20, 5);



// Item 2

function printProduct(num1, num2){
	let product = num1 * num2;
	console.log("The product of 50 and 10:");
	console.log(product);
};
	function printQuotient(num1, num2){
		let quotient = num1 / num2;
		console.log("The quotient of 50 and 10:");
		console.log(quotient);
};
printProduct(50, 10);
printQuotient(50, 10);



// Item 3

function printArea(num){
	let circleArea = (num * num) * 3.1416;
	console.log("The result of getting the area of a circle with 15 radius: ");
	console.log(circleArea);
};
printArea(15);



// Item 4

function printAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	console.log("The average of " + num1 + ", " + num2 + ", " +  num3 + " and " +  num4 + ": ");
	console.log(average);
};
printAverage(20, 40, 60, 80);



// Item 5

function printPassed(num1, num2){
	let percentage = (num1 / num2) * 100;
	let isPassed = percentage >= 75;
	console.log("Is 38/50 a passing score? ");
	console.log(isPassed);
};
printPassed(38, 50);